﻿using Moq;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using wooliesx_harvey.Models;
using wooliesx_harvey.Services;
using Xunit;

namespace wooliesx_harvey_tests.Services
{
    public class SortServiceTests
    {
        private Mock<IProductService> mockProductService;
        private SortService sortService;

        public SortServiceTests()
        {
            var productPopularityDict = new Dictionary<string, long> {
                { "A", 1 },
                { "Z", 15 },
                { "F", 30 },
                { "C", 999 },
                { "B", 3 },
                { "D", 6 }
            };

            mockProductService = new Mock<IProductService>();
            mockProductService
                .Setup(ps => ps.GetProductPurchaseCount())
                .ReturnsAsync(productPopularityDict);

            sortService = new SortService(mockProductService.Object);
        }

        [Fact]
        public async Task ShouldThrowArgumentExceptionIfInvalidSortOptionIsProvided()
        {
            // Act
            //Assert
            await Assert.ThrowsAsync<ArgumentException>(
                () => sortService.Sort(getProductList(), (SortOption)9)
            );
        }

        [Fact]
        public async Task ShouldSortByPriceLowToHighCorrectly()
        {
            // Act
            var sortedProducts = await sortService.Sort(getProductList(), SortOption.Low);

            //Assert
            var expectedOrder = new List<string> { "B", "A", "C", "Z", "D", "F" };
            assertOrder(expectedOrder, sortedProducts);
        }

        [Fact]
        public async Task ShouldSortByPriceHighToLowCorrectly()
        {
            // Act
            var sortedProducts = await sortService.Sort(getProductList(), SortOption.High);

            //Assert
            var expectedOrder = new List<string> { "F", "D", "Z", "C", "A", "B" };
            assertOrder(expectedOrder, sortedProducts);
        }

        [Fact]
        public async Task ShouldSortByNameAscendingCorrectly()
        {
            // Act
            var sortedProducts = await sortService.Sort(getProductList(), SortOption.Ascending);

            //Assert
            var expectedOrder = new List<string> { "A", "B", "C", "D", "F", "Z" };
            assertOrder(expectedOrder, sortedProducts);
        }

        [Fact]
        public async Task ShouldSortByNameDescendingCorrectly()
        {
            // Act
            var sortedProducts = await sortService.Sort(getProductList(), SortOption.Descending);

            //Assert
            var expectedOrder = new List<string> { "Z", "F", "D", "C", "B", "A" };
            assertOrder(expectedOrder, sortedProducts);
        }

        [Fact]
        public async Task ShouldSortByProductsWithHighestPopularity()
        {
            // Act
            var sortedProducts = await sortService.Sort(getProductList(), SortOption.Recommended);

            //Assert
            var expectedOrder = new List<string> { "C", "F", "Z", "D", "B", "A" };
            assertOrder(expectedOrder, sortedProducts);
        }

        private void assertOrder(List<string> expectedProductNames, List<Product> productList)
        {
            for (var i = 0; i < productList.Count - 1; i++)
            {
                Assert.Equal(expectedProductNames[i], productList[i].Name);
            }
        }

        private List<Product> getProductList()
        {
            return new List<Product>
            {
                new Product
                {
                    Name = "A",
                    Price = 99.99,
                    Quantity = 3000
                },
                new Product
                {
                Name = "B",
                    Price = 0.5,
                    Quantity = 88
                },
                new Product
                {
                    Name = "C",
                    Price = 101.98,
                    Quantity = 3
                },
                new Product
                {
                    Name = "D",
                    Price = 10199,
                    Quantity = 503
                },
                new Product
                {
                    Name = "Z",
                    Price = 101.99,
                    Quantity = 3
                },
                new Product
                {
                    Name = "F",
                    Price = 9999999,
                    Quantity = 1
                }
            };
        }
    }
}
