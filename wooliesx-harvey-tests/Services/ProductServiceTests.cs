﻿using Moq;
using System.Collections.Generic;
using System.Threading.Tasks;
using wooliesx_harvey.Models;
using wooliesx_harvey.Services;
using Xunit;

namespace wooliesx_harvey_tests.Services
{
    public class ProductServiceTests
    {
        [Fact]
        public async Task ShouldReturnCorrectProductPurchaseCountDict()
        {
            // Arrange
            var fakeHistory = getFakeHistory();
            var mockApi = generateMockApi(fakeHistory);
            var productService = new ProductService(mockApi.Object);

            // Act
            var productHistoryRes = await productService.GetProductPurchaseCount();

            //Assert
            Assert.Equal(6, productHistoryRes.Count);
            Assert.Equal(18, productHistoryRes["Test Product A"]);
            Assert.Equal(12, productHistoryRes["Test Product E"]);
            Assert.Equal(7, productHistoryRes["Test Product Z"]);
            Assert.Equal(7, productHistoryRes["Test Product FFF"]);
            Assert.Equal(1, productHistoryRes["Test Product B"]);
            Assert.Equal(1, productHistoryRes["Test Product F"]);
        }

        private Mock<IWooliesApiService> generateMockApi(List<ShopperHistory> shopperHistory)
        {
            var wooliesApiServiceMock = new Mock<IWooliesApiService>();
            wooliesApiServiceMock
                .Setup(api => api.GetShopperHistory())
                .ReturnsAsync(shopperHistory);

            return wooliesApiServiceMock;
        }

        private List<ShopperHistory> getFakeHistory()
        {
            return new List<ShopperHistory> {
                new ShopperHistory {
                    CustomerId = 123,
                    Products = new List<Product> {
                        new Product
                        {
                            Name = "Test Product A",
                            Price = 99.99,
                            Quantity = 3
                        },
                        new Product
                        {
                            Name = "Test Product B",
                            Price = 101.99,
                            Quantity = 1
                        },
                        new Product
                        {
                            Name = "Test Product F",
                            Price = 999999999999,
                            Quantity = 1
                        },
                    }
                },
                new ShopperHistory {
                    CustomerId = 45,
                    Products = new List<Product> {
                        new Product
                        {
                            Name = "Test Product A",
                            Price = 53312,
                            Quantity = 5
                        },
                        new Product
                        {
                            Name = "Test Product E",
                            Price = 101.99,
                            Quantity = 2
                        },
                        new Product
                        {
                            Name = "Test Product Z",
                            Price = 999999999999,
                            Quantity = 7
                        },
                    }
                },
                 new ShopperHistory {
                    CustomerId = 1,
                    Products = new List<Product> {
                        new Product
                        {
                            Name = "Test Product A",
                            Price = 53312,
                            Quantity = 10
                        },
                        new Product
                        {
                            Name = "Test Product E",
                            Price = 101.99,
                            Quantity = 10
                        },
                        new Product
                        {
                            Name = "Test Product FFF",
                            Price = 2,
                            Quantity = 7
                        },
                    }
                }
            };
        }
    }
}
