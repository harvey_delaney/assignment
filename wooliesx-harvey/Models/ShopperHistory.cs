﻿using System.Collections.Generic;

namespace wooliesx_harvey.Models
{
    public class ShopperHistory
    {
        public int CustomerId { get; set; }
        public List<Product> Products { get; set; }
    }
}
