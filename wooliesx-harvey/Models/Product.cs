﻿namespace wooliesx_harvey.Models
{
    public class Product
    {
        public string Name { get; set; }
        public double Price { get; set; }
        public long Quantity { get; set; }
    }
}
