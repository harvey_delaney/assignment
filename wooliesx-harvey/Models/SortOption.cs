﻿namespace wooliesx_harvey.Models
{
    public enum SortOption
    {
        Low = 0,
        High,
        Ascending,
        Descending,
        Recommended
    }
}
