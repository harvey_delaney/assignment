﻿using System.Collections.Generic;

namespace wooliesx_harvey.Models
{
    public class Specials
    {
        public List<QuantityModel> Quantities { get; set; }
        public long Total { get; set; }
    }
}
