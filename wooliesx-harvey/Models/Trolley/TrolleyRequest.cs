﻿using System.Collections.Generic;

namespace wooliesx_harvey.Models
{
    public class TrolleyRequest
    {
        public List<Product> Products { get; set; }
        public List<Specials> Specials { get; set; }
        public List<QuantityModel> Quantities { get; set; }
    }
}
