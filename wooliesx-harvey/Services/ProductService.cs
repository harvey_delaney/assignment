﻿using System.Collections.Generic;
using System.Threading.Tasks;

public interface IProductService
{
    Task<Dictionary<string, long>> GetProductPurchaseCount();
}

namespace wooliesx_harvey.Services
{
    public class ProductService: IProductService
    {
        private readonly IWooliesApiService _wooliesApiService;

        public ProductService(IWooliesApiService wooliesApiService)
        {
            _wooliesApiService = wooliesApiService;
        }

        public async Task<Dictionary<string, long>> GetProductPurchaseCount()
        {
            var productCountDict = new Dictionary<string, long>();
            var shopperHistory = await _wooliesApiService.GetShopperHistory();

            shopperHistory.ForEach(history =>
            {
                history.Products.ForEach(product =>
                {
                    if (productCountDict.TryGetValue(product.Name, out var count))
                    {
                        productCountDict[product.Name] += product.Quantity;
                    }
                    else
                    {
                        productCountDict[product.Name] = product.Quantity;
                    }
                });
            });

            return productCountDict;
        }
    }
}
