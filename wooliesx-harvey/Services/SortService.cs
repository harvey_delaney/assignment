﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using wooliesx_harvey.Models;

public interface ISortService
{
    Task<List<Product>> Sort(List<Product> productList, SortOption sortOption);
}

namespace wooliesx_harvey.Services
{
    public class SortService: ISortService
    {
        private Dictionary<SortOption, Func<List<Product>, List<Product>>> _sortStrategyDict;
        private Dictionary<SortOption, Func<List<Product>, Task<List<Product>>>> _asyncSortStrategyDict;
        private IProductService _productService;

        public SortService(IProductService productService)
        {
            _productService = productService;

            _sortStrategyDict = new Dictionary<SortOption, Func<List<Product>, List<Product>>>
            {
                {  SortOption.Low, _sortLowToHigh },
                {  SortOption.High, _sortHighToLow },
                {  SortOption.Ascending, _sortAscending },
                {  SortOption.Descending, _sortDescending },
            };

            _asyncSortStrategyDict = new Dictionary<SortOption, Func<List<Product>, Task<List<Product>>>>
            {
                {  SortOption.Recommended, _sortRecommended }
            };
        }

        public async Task<List<Product>> Sort(List<Product> productList, SortOption sortOption)
        {
            if (_sortStrategyDict.TryGetValue(sortOption, out var sortFunction))
            {
                return sortFunction(productList);
            }

            if (_asyncSortStrategyDict.TryGetValue(sortOption, out var asyncSortFunction))
            {
                return await asyncSortFunction(productList);
            }

            throw new ArgumentException("Invalid sort option provided");
        }

        private List<Product> _sortLowToHigh(List<Product> productList)
        {
            var sortedList = productList
                .OrderBy(p => p.Price)
                .ToList();

            return sortedList;
        }

        private List<Product> _sortHighToLow(List<Product> productList)
        {
            var sortedList = productList
                .OrderByDescending(p => p.Price)
                .ToList();

            return sortedList;
        }

        private List<Product> _sortAscending(List<Product> productList)
        {
            var sortedList = new List<Product>(productList);

            sortedList.Sort((a, b) => string.Compare(a.Name, b.Name));

            return sortedList;
        }

        private List<Product> _sortDescending(List<Product> productList)
        {
            var sortedList = new List<Product>(productList);

            sortedList.Sort((a, b) => string.Compare(b.Name, a.Name));

            return sortedList;
        }

        public async Task<List<Product>> _sortRecommended(List<Product> productList)
        {
            var productCountDict = await _productService.GetProductPurchaseCount();
            var newList = new List<Product>(productList);

            newList.Sort((a, b) =>
            {
                productCountDict.TryGetValue(a.Name, out var aCount);
                productCountDict.TryGetValue(b.Name, out var bCount);

                return bCount.CompareTo(aCount);
            });

            return newList;
        }
    }
}
