﻿using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace wooliesx_harvey.Services
{
    public class ApiService : HttpClient
    {
        private readonly HttpClient _client;

        public ApiService(HttpClient client, string baseUrl)
        {
            client.BaseAddress = new Uri(baseUrl);
            client.DefaultRequestHeaders.Add("Accept", "application/json");

            _client = client;
        }

        protected async Task<T> get<T>(string url) {
            var res = await _client.GetAsync($"{url}");

            res.EnsureSuccessStatusCode();

            var jsonString = await res.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(jsonString);
        }

        protected async Task<T> post<T>(string url, object body)
        {
            var res = await _client.PostAsJsonAsync($"{url}", body);

            res.EnsureSuccessStatusCode();

            var jsonString = await res.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(jsonString);
        }
    }
}