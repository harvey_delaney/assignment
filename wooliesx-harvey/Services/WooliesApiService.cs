﻿using Microsoft.Extensions.Configuration;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using wooliesx_harvey.Models;

namespace wooliesx_harvey.Services
{
    public interface IWooliesApiService
    {
        Task<List<Product>> GetProducts();
        Task<List<ShopperHistory>> GetShopperHistory();
        Task<long> GetTrolleyTotal(TrolleyRequest trolleyRequest);
    }

    public class WooliesApiService: ApiService, IWooliesApiService
    {
        private string apiToken;

        public WooliesApiService(HttpClient client, IConfiguration configuration): 
            base(client, configuration.GetValue<string>("wooliesApiBaseUrl"))
        {
            apiToken = configuration.GetValue<string>("token");
        }

        public async Task<List<Product>> GetProducts()
        {
            return await get<List<Product>>($"products?token={apiToken}");
        }

        public async Task<List<ShopperHistory>> GetShopperHistory()
        {
            return await get<List<ShopperHistory>>($"shopperHistory?token={apiToken}");
        }

        public async Task<long> GetTrolleyTotal(TrolleyRequest trolleyRequest)
        {
            return await post<long>($"trolleyCalculator?token={apiToken}", trolleyRequest);
        }
    }
}