﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using wooliesx_harvey.Models;
using wooliesx_harvey.Services;

namespace wooliesx_harvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SortController : ControllerBase
    {
        private readonly ISortService _sortService;
        private readonly IWooliesApiService _wooliesApiService;

        public SortController(ISortService sortService, IWooliesApiService productService)
        {
            _sortService = sortService;
            _wooliesApiService = productService;
        }

        [HttpGet("{sortOption}")]
        public async Task<ActionResult<List<Product>>> Sort(SortOption sortOption)
        {
            var products = await _wooliesApiService.GetProducts();
            var sortedProducts = await _sortService.Sort(products, sortOption);

            return Ok(sortedProducts);
        }
    }
}
