﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using wooliesx_harvey.Models;

namespace wooliesx_harvey.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public UserController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        public ActionResult<UserResponse> Get()
        {
            var response = new UserResponse
            {
                Name = _configuration.GetValue<string>("name"),
                Token = _configuration.GetValue<string>("token")
            };

            return Ok(response);
        }
    }
}
