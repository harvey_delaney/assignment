﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using wooliesx_harvey.Models;
using wooliesx_harvey.Services;

namespace wooliesx_harvey.Controllers
{
    [Route("api")]
    [ApiController]
    public class TrolleyTotalController : ControllerBase
    {
        private readonly IWooliesApiService _wooliesApiService;

        public TrolleyTotalController(IWooliesApiService wooliesApiService)
        {
            _wooliesApiService = wooliesApiService;
        }

        [HttpPost("trolleyTotal")]
        public async Task<ActionResult<long>> TrolleyTotal(TrolleyRequest trolleyRequest)
        {
            var res = await _wooliesApiService.GetTrolleyTotal(trolleyRequest);

            return Ok(res);
        }
    }

}
