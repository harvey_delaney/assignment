# Harvey's Attemp at the Woolies Dev Challenge
Hey thanks for reviewing my go at the challenge.

Didn't get around to completing my own implementation of exercise 3 (it's a tricky one!) Also didn't get around to hosting this project. The steps I'd follow to get it up and running would be:

1: Create Dockerfile for this .NET Core project.

2: Clone repo on my box (currently running https://www.harveydelaney.com)

3: Add an entry into my docker-compose.yaml file for this project.

4: Add subdomain entry on Namecheap -> woolies.harveydelaney.com

5: Create new docker image and run container then run the woolies UI test.

I've written a few blogs that have gotten a bit popular if you want to read more about my experience around hosting websites:

https://blog.harveydelaney.com/hosting-websites-using-docker-nginx/

https://blog.harveydelaney.com/jenkins-build-test-deploy-node-app/

Again, thanks for reviewing and would appreciate feedback.
